package lang

import (
	"iering.com/webcore/core.zq/def"

	log "gopkg.in/clog.v1"
	"gopkg.in/ini.v1"
)

var langMap map[string]string

func NewContext() {
	langFile := def.Language.Locales["locale_"+def.Language.Current+".ini"]

	config, err := ini.Load(langFile)
	if err != nil {
		log.Fatal(3, "Load config fail! %#v", err)
	}
	langMap = config.Section("").KeysHash()
}

func Get(keyname string) string {
	value, ok := langMap[keyname]
	if ok {
		return value
	}
	return ""
}
