package email

import (
	"fmt"
	"net/smtp"
	"strings"

	log "gopkg.in/clog.v1"

	"iering.com/webcore/core.zq/def"
)

type Mail struct {
	Addr        string
	Auth        smtp.Auth
	Name        string
	From        string
	ContentType string
}

func New() *Mail {
	m := Mail{}
	m.Addr = fmt.Sprintf("%s:%d", def.Email.Server, def.Email.ServerPort)
	m.Auth = smtp.PlainAuth("", def.Email.Account, def.Email.Password, def.Email.Server)
	m.From = def.Email.Name
	m.From = def.Email.Account

	if def.Email.ContentType == "html" {
		m.ContentType = "Content-Type: text/html; charset=UTF-8"
	} else {
		m.ContentType = "Content-Type: text/plain; charset=UTF-8"
	}

	return &m
}

func (m *Mail) Send(to []string, subject, body string) bool {
	msg := fmt.Sprintf("To: %s\r\nFrom: %s<%s>\r\nSubject: %s\r\n%s\r\n\r\n%s",
		strings.Join(to, ","),
		m.Name,
		m.From,
		subject,
		m.ContentType,
		body)

	err := smtp.SendMail(m.Addr, m.Auth, m.From, to, []byte(msg))
	if err != nil {
		log.Error(1, "send email error %+v", err)
		return false
	}

	return true
}

func (m *Mail) SendAccountRegister(to, body string) bool {
	return m.Send([]string{to}, "", body)
}
