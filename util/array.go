package util

/**
 * 判断数组或者slice是否包含某个元素
 */
func InArray(values []string, value string) bool {
	for _, val := range values {
		if val == value {
			return true
		}
	}

	return false
}
