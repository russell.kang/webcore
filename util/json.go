package util

import (
	"encoding/json"

	log "gopkg.in/clog.v1"
)

func EncodeJson(data interface{}) string {
	b, err := json.Marshal(data)
	if err != nil {
		log.Warn("controller WriteJson: %#v", err)
	}
	return string(b)
}

func DecodeToMapInt(result string) map[string]int {
	info := map[string]int{}

	err := json.Unmarshal([]byte(result), &info)
	if err != nil {
		log.Warn("util DecodeToMapInt: %#v", err)
	}

	return info
}

func DecodeToMapInts(result string) []map[string]int {
	info := []map[string]int{}

	err := json.Unmarshal([]byte(result), &info)
	if err != nil {
		log.Warn("util DecodeToMapInt: %#v", err)
	}

	return info
}

func DecodeToMapString(result string) map[string]string {
	info := map[string]string{}

	err := json.Unmarshal([]byte(result), &info)
	if err != nil {
		log.Warn("util DecodeToMapString: %#v", err)
	}

	return info
}

func DecodeToStruct(result string, info interface{}) {
	err := json.Unmarshal([]byte(result), info)
	if err != nil {
		log.Warn("util DecodeToStruct: %#v", err)
	}
}
