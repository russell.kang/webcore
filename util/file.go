package util

import (
	"os"
)

// 如果给定路径是文件，则IsFile返回true
// 当它是目录或不存在时返回false。
func IsFile(filePath string) bool {
	f, e := os.Stat(filePath)
	if e != nil {
		return false
	}
	return !f.IsDir()
}

// IsExist检查文件或目录是否存在。
//当文件或目录不存在时返回false。
func IsExist(path string) bool {
	_, err := os.Stat(path)
	return err == nil || os.IsExist(err)
}
