package util

import (
	"fmt"
	"strconv"

	log "gopkg.in/clog.v1"
)

func StrToFloat64(str string) float64 {
	if len(str) <= 0 {
		return 0
	}

	floatValue, err := strconv.ParseFloat(str, 64)
	if err != nil {
		log.Warn("util StrToFloat64: %#v", err)
		return 0
	}
	return floatValue
}

func StrToInt(str string) int {
	if len(str) <= 0 {
		return 0
	}

	intValue, err := strconv.Atoi(str)
	if err != nil {
		log.Warn("util StrToInt: %#v", err)
		return 0
	}
	return intValue
}

func StrToInt64(str string) int64 {
	if len(str) <= 0 {
		return 0
	}

	intValue, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		log.Warn("util StrToInt64: %#v", err)
		return 0
	}

	return intValue
}

/**
 * 金额处理，10000分 == 100.00元
 */
func Percentile(money string) string {
	return fmt.Sprintf("%.2f", StrToFloat64(money)/100)
}

/**
 * 金额处理，10000分 == 100.00元
 */
func PercentileFloat(money float64) float64 {
	return StrToFloat64(fmt.Sprintf("%.2f", money/100))
}
