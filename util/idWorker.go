package util

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	. "iering.com/webcore/core.zq/constant"
)

/**
 * 订单编号
 */
func IdWorker(machine string) string {
	orderTime := time.Now()
	return fmt.Sprintf("%s%d%d",
		machine,
		orderTime.UnixNano(),
		rand.Intn(999),
	)
}

/**
 * dateInt 与当前时间差
 */
func LeftTime(dateInt int64) int {
	objTime := time.Unix(dateInt, 0)

	return int(objTime.Sub(time.Now()).Seconds())
}

/**
 * 时间戳 转为 日期
 */
func FormatTime(dateInt int64) string {
	objTime := time.Unix(dateInt, 0)

	return objTime.Format(TIME_FORMAT_REGULAR_SECOND)
}

/**
 * 时间戳 转为 日期
 */
func FormatTimeByString(dateStr string) string {
	objTime, err := time.Parse(TIME_FORMAT_REGULAR, dateStr)
	if err != nil {
		return ""
	}

	return objTime.Format(TIME_FORMAT_REGULAR_SECOND)
}

/**
 * 唯一编号
 */
func NumberWorker() string {
	nowTime := time.Now()
	return fmt.Sprintf("%s%s%s%s%d",
		nowTime.Format("06"),               // 2位年份
		PadLeft(nowTime.YearDay(), 3, "0"), // 3位天数
		PadLeft(nowTime.Hour(), 2, "0"),    // 2位小时
		PadLeft(nowTime.Minute(), 2, "0"),  // 2位分钟
		rand.Intn(999),
		// PadLeft(nowTime.Second(), 2, "0"),  // 2位秒
		// nowTime.Nanosecond(),               //那一秒内的纳秒
	)
}

/**
 * 反回固定长度字符串
 * @param  {[type]} str interface{} 原字符串
 * @param  {[type]} len int         需要返回的长度
 * @param  {[type]} pad string      填充的字符
 * @return {[type]} string          返回字符串
 */
func PadLeft(str int, oringLen int, pad string) string {
	strLen := len(strconv.Itoa(str))
	if strLen < oringLen {
		return fmt.Sprintf("%s%d", strings.Repeat(pad, oringLen-strLen), str)
	}
	return fmt.Sprintf("%#v", str)
}

/**
 * 随机字符串
 */
func RandString(length int) string {
	r := rand.New(rand.NewSource(time.Now().Unix()))

	bytes := make([]byte, length)
	for i := 0; i < length; i++ {
		bytes[i] = byte(r.Intn(26) + 65)
	}

	return string(bytes)
}

/**
 * 随机字符串 --- 数字
 */
func RandInt(length int) string {
	r := rand.New(rand.NewSource(time.Now().Unix()))

	bytes := make([]byte, length)
	for i := 0; i < length; i++ {
		bytes[i] = byte(r.Intn(10))
	}

	return string(bytes)
}
