package util

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/subtle"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"time"
)

func Md5(str string) string {
	h := md5.New()
	h.Write([]byte(str)) // 需要加密的字符串为 123456
	cipherStr := h.Sum(nil)
	return hex.EncodeToString(cipherStr) // 输出加密结果
}

func Sha1(str string) string {
	h := sha1.New()
	h.Write([]byte(str)) // 需要加密的字符串为 123456
	cipherStr := h.Sum(nil)
	return hex.EncodeToString(cipherStr) // 输出加密结果
}

/**
 *
log.Info(auth.MakePassword("123123"))

verfiyPassword := auth.VerfiyPassword("123123", "96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e")
fmt.Println(verfiyPassword)
*/
func VerfiyPassword(password, salt, pw string) bool {
	newPassword := EncryptPassword(password, salt)

	return subtle.ConstantTimeCompare([]byte(newPassword), []byte(pw)) == 1
}
func EncryptPassword(password, salt string) string {
	value := sha256.Sum256([]byte(salt + password + salt))

	return fmt.Sprintf("%x", value)
}

func GenerateToken(uid int64) string {
	hashString := fmt.Sprintf("%d%d", uid, time.Now().Unix())

	return fmt.Sprintf("%x", sha256.Sum256([]byte(hashString)))
}

/**
 * Md5File
 * @param {[type]} path string) (string, error [description]
 */
func Md5File(file multipart.File) ([]byte, string, error) {
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return fileBytes, "", err
	}

	fileHashString := fmt.Sprintf("%x", md5.Sum(fileBytes))

	return fileBytes, fileHashString, nil
}

/**
 * SHA1File
 * @param {[type]} file multipart.File) (io.Reader, string [description]
 */
func SHA1File(file multipart.File) ([]byte, string, error) {
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return fileBytes, "", err
	}

	fileHashString := fmt.Sprintf("%x", sha1.Sum(fileBytes))

	return fileBytes, fileHashString, nil
}

/**
 * SHA256File
 * @param {[type]} file multipart.File) (io.Reader, string [description]
 */
func SHA256File(file multipart.File) ([]byte, string, error) {
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return fileBytes, "", err
	}

	fileHashString := fmt.Sprintf("%x", sha256.Sum256(fileBytes))

	return fileBytes, fileHashString, nil
}
