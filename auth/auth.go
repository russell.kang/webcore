package auth

import (
	"github.com/dgryski/dgoogauth"
	log "gopkg.in/clog.v1"
)

func Verify(code, secret string) bool {
	otpconf := &dgoogauth.OTPConfig{
		Secret:      secret,
		WindowSize:  3,
		HotpCounter: 0,
	}

	result, err := otpconf.Authenticate(code)
	if err != nil {
		log.Warn("Authenticate Error: %#v", err)
	}
	return result
}
