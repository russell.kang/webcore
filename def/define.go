package def

import (
	"strings"

	"iering.com/webcore/core.zq/util"

	log "gopkg.in/clog.v1"
	"gopkg.in/ini.v1"
)

var (
	AppVer string

	ProdMode   bool
	LogFile    string
	HttpAddr   string
	HttpPort   int
	IsEnableIp bool
	EnableIps  string

	PageSize    int
	TokenExpired int

	Server struct {
		Name        string
		Desc        string
		URL         string
		DataUrl    string
		DataFolder string
	}

	Db struct {
		Driver string
		Dsn    string
	}

	Redis struct {
		HostServer string
		Password   string
		Db         int
	}

	Language struct {
		Langs   []string
		Current string
		Locales map[string]string
	}


	Payment struct {
		WxAppId  string
		WxMchId  string
		WxAppKey string
	}

	Email struct {
		Server      string
		ServerPort  int
		Account     string
		Password    string
		Name        string
		ContentType string
	}

	Cfg *ini.File
)

func NewContext() {
	sources := []interface{}{"conf/app.ini"}

	var err error
	Cfg, err = ini.Load(sources[0], sources[1:]...)
	if err != nil {
		log.Fatal(2, "Fail to load config: %#v", err)
	}

	sec := Cfg.Section("")
	Server.Name = sec.Key("NAME").MustString("Peach Server")
	Server.Desc = sec.Key("DESC").String()

	sec = Cfg.Section("server")
	ProdMode = sec.Key("RUN_MODE").String() == "prod"
	LogFile = sec.Key("LOG_FILE").String()
	HttpAddr = sec.Key("HTTP_ADDR").MustString("0.0.0.0")
	HttpPort = sec.Key("HTTP_PORT").MustInt(5555)
	IsEnableIp = sec.Key("ENABLE_IP").MustBool()
	EnableIps = sec.Key("ENABLE_IPADDRS").String()

	Server.URL = sec.Key("URL").String()
	Server.DataUrl = sec.Key("DATA_URL").String()
	Server.DataFolder = sec.Key("DATA_FOLDER").String()

	PageSize = sec.Key("PAGE_SIZE").MustInt(10)
	TokenExpired = sec.Key("TOKEN_EXPIRED").MustInt(0)

	sec = Cfg.Section("db")
	Db.Driver = sec.Key("driver").String()
	Db.Dsn = sec.Key("dsn").String()

	sec = Cfg.Section("redis")
	Redis.HostServer = sec.Key("HostServer").MustString("127.0.0.1:6379")
	Redis.Password = sec.Key("Password").MustString("")
	Redis.Db = sec.Key("Db").MustInt(0)

	lang := Cfg.Section("lang")
	Language.Langs = lang.Key("LANGS").Strings(",")
	Language.Current = lang.Key("LANG").String()
	Language.Locales = make(map[string]string)
	dir := lang.Key("CUSTOM_DIRECTORY").MustString("conf/locale")
	for _, l := range Language.Langs {
		langFile := dir + "/locale_" + l + ".ini"
		if util.IsFile(langFile) {
			Language.Locales["locale_"+l+".ini"] = langFile
		} else {
			Language.Locales["locale_"+l+".ini"] = ""
		}
	}

	sec = Cfg.Section("payment")
	Payment.WxAppId = sec.Key("Wx_AppId").String()
	Payment.WxMchId = sec.Key("Wx_MchId").String()
	Payment.WxAppKey = sec.Key("Wx_AppKey").String()

	sec = Cfg.Section("email")
	Email.Server = sec.Key("Server").String()
	Email.ServerPort = sec.Key("ServerPort").MustInt(465)
	Email.Account = sec.Key("Account").String()
	Email.Password = sec.Key("Password").String()
	Email.Name = sec.Key("Name").String()
	Email.ContentType = sec.Key("ContentType").String()
}

func init() {
	log.New(log.CONSOLE, log.ConsoleConfig{})
}

/**
 * 检测IP 是否是禁止的， 不是返回false
 */
func DisableIp(currentIp string) bool {
	if !IsEnableIp {
		log.Warn("IsEnableIp: %v", IsEnableIp)
		return false
	}

	ips := strings.Split(EnableIps, ",")
	for _, ip := range ips {
		if ip == currentIp {
			return false
		}
	}

	log.Warn("DisableIp: %v", currentIp)
	return true
}
