package controller

import (
	"fmt"
	"regexp"
)

const (
	Number = "[0-9]"
	String = "[a-zA-Z]"
)

func ValidEmail(email string) bool {
	reg := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	return reg.MatchString(email)
}

func ValidPhone(phone string) bool {
	reg := regexp.MustCompile("^1[0-9]{10}$")

	return reg.MatchString(phone)
}

func ValidRange(str string, length int, limit int) bool {
	lenStr := len(str)
	if lenStr >= length && lenStr <= limit {
		return true
	}
	return false
}

func ValidEqual(str string, length int, vtype string) bool {
	switch vtype {
	case Number:
		reg := regexp.MustCompile(fmt.Sprintf("%s{%d}", Number, length))
		return reg.MatchString(str)

	case String:
		reg := regexp.MustCompile(fmt.Sprintf("%s{%d}", String, length))
		return reg.MatchString(str)

	default:
		if len(str) == length {
			return true
		}
	}
	return false
}

func ValidEmpty(str string) bool {
	return len(str) > 0
}
