package controller

import (
	"strconv"
	"strings"

	log "gopkg.in/clog.v1"
)

// 获取http GET 数据
func (c *Controller) GetValue(key string) cString {
	return cString(strings.TrimSpace(c.Request.FormValue(key)))
}

// 获取http POST 数据
func (c *Controller) PostValue(key string) cString {
	return cString(strings.TrimSpace(c.Request.PostFormValue(key)))
}

func (s cString) Int() int {
	if len(s) <= 0 {
		return 0
	}

	intValue, err := strconv.Atoi(string(s))
	if err != nil {
		log.Warn("util StrInt: %#v", err)
		return 0
	}
	return intValue
}
func (s cString) MustInt(value int) int {
	if len(s) <= 0 {
		return value
	}

	intValue, err := strconv.Atoi(string(s))
	if err != nil {
		log.Warn("util StrInt: %#v", err)
		return 0
	}
	return intValue
}
func (s cString) Int64() int64 {
	if len(s) <= 0 {
		return 0
	}

	intValue, err := strconv.ParseInt(string(s), 10, 64)
	if err != nil {
		log.Warn("util toInt64: %#v", err)
		return 0
	}
	return intValue
}
func (s cString) MustInt64(value int64) int64 {
	if len(s) <= 0 {
		return value
	}

	intValue, err := strconv.ParseInt(string(s), 10, 64)
	if err != nil {
		log.Warn("util StrInt: %#v", err)
		return 0
	}
	return intValue
}

func (s cString) Float64() float64 {
	if len(s) <= 0 {
		return 0
	}

	floatValue, err := strconv.ParseFloat(string(s), 64)
	if err != nil {
		log.Warn("util ToFloat64: %#v", err)
		return 0
	}
	return floatValue
}
func (s cString) MustFlost64(value float64) float64 {
	if len(s) <= 0 {
		return value
	}

	floatValue, err := strconv.ParseFloat(string(s), 64)
	if err != nil {
		log.Warn("util StrInt: %#v", err)
		return 0
	}
	return floatValue
}
