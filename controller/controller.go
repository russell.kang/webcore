package controller

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"strings"
	"time"

	. "iering.com/webcore/core.zq/constant"
	"iering.com/webcore/core.zq/lang"
	"iering.com/webcore/core.zq/token"
	"iering.com/webcore/core.zq/util"

	log "gopkg.in/clog.v1"
)

type (
	Controller struct {
		Response http.ResponseWriter
		Request  *http.Request
		Result   Result
		Channel  string
	}
	Result struct {
		Code    int         `json:"code"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	cString string
)

func New(w http.ResponseWriter, r *http.Request) *Controller {
	c := &Controller{}
	c.Response = w
	c.Request = r

	// 解析url传递的参数
	c.Request.ParseForm()

	return c
}

// 获取http GET 数据
func (c *Controller) Get(key string) string {
	return strings.TrimSpace(c.Request.FormValue(key))
}

// 获取http POST 数据
func (c *Controller) Post(key string) string {
	return strings.TrimSpace(c.Request.PostFormValue(key))
}

// 获取http Header 数据
func (c *Controller) Header(key string) string {
	return strings.TrimSpace(c.Request.Header.Get(key))
}

func (c *Controller) CheckToken() bool {
	code := c.HasChannel()
	if code != CODE_OK {
		c.SetCode(code)
		return false
	}

	tokenString := c.Post("token")
	code = token.Has(tokenString, c.Channel)

	if code != CODE_OK {
		c.SetCode(code)
		return false
	}

	return true
}
func (c *Controller) CheckTokenBy(tokenString string) bool {
	code := c.HasChannel()
	if code != CODE_OK {
		c.SetCode(code)
		return false
	}

	code = token.Has(tokenString, c.Channel)

	if code != CODE_OK {
		c.SetCode(code)
		return false
	}

	return true
}

func (c *Controller) CheckManagerToken() bool {
	tokenString := c.Post("token")
	code := token.Has(tokenString, CHANNEL_MGR)

	if code != CODE_OK {
		c.SetCode(code)
		return false
	}

	return true
}

// 获取 Token 相关信息
func (c *Controller) GetByToken(key string) int64 {
	tokenString = c.Post("token")
	return util.StrToInt64(token.GetField(tokenString, key))
}

// 获取客户端IP
func (c *Controller) GetIp() string {
	var currentIp string = c.Header("XRemoteAddr")
	if len(currentIp) == 0 {
		currentIp = c.Request.RemoteAddr[0:strings.Index(c.Request.RemoteAddr, ":")]
	}

	return currentIp
}

// 获取客户端User-Agent
func (c *Controller) HasChannel() int {
	if c.checkChannel(CHANNEL_IOS, SECRET_IOS) || c.checkChannel(CHANNEL_ANDROID, SECRET_ANDROID) {
		return CODE_OK
	}

	c.Channel = ""
	return CODE_CHANNEL
}

// 检查
func (c *Controller) checkChannel(channel, secret string) bool {
	uas := strings.Split(c.Request.UserAgent(), " ")

	log.Warn("UserAgent : %+v", uas)

	if len(uas) != 4 {
		return false
	}
	c.Channel = uas[0]

	secretString := util.Md5(secret + uas[2])
	return (uas[0] == channel) && (uas[3] == util.Md5(fmt.Sprintf("%s%s%s%s%s", uas[0], uas[1], uas[2], secretString, secret)))
}

// 生成UserAgent
func generateUserAgent(currentChannel string) (string, string, int) {
	var signs [4]string

	signs[0] = CHANNEL_WEB                                                                                 // channel
	signs[1] = fmt.Sprintf("%d", time.Now().Unix())                                                        // ts
	signs[2] = fmt.Sprintf("%s.%s", util.Sha1(signs[1]), currentChannel)                                   // nonce
	secretString := util.Md5(SECRET_WEB + signs[2])                                                        // secret string
	signs[3] = util.Md5(fmt.Sprintf("%s%s%s%s%s", signs[0], signs[1], signs[2], secretString, SECRET_WEB)) // sign

	log.Warn("signs : %+v", signs)

	return currentChannel, fmt.Sprintf("%s %s %s %s", signs[0], signs[1], signs[2], signs[3]), CODE_OK
}

func (c *Controller) SetCode(code int) {
	c.Result.Code = code
	if len(c.Result.Message) == 0 {
		c.Result.Message = lang.Get(fmt.Sprintf("%d", code))
	}
}
func (c *Controller) SetMessage(msg string) {
	c.Result.Message = msg
}
func (c *Controller) SetData(data interface{}) {
	c.Result.Data = data
}
func (c *Controller) Set(code int, msg string, data interface{}) {
	c.Result.Code = code
	c.Result.Message = msg
	c.Result.Data = data
}

func (c *Controller) WriteJson() {
	b, err := json.Marshal(c.Result)
	if err != nil {
		log.Warn("controller WriteJson: %#v", err)
		return
	}
	c.Response.Header().Set("Content-Type", "application/json")
	c.Response.WriteHeader(http.StatusOK)
	c.Response.Write(b)
}

func (c *Controller) Write(out interface{}) {
	b, err := json.Marshal(out)
	if err == nil {
		c.Response.Header().Set("Content-Type", "application/json")
		c.Response.WriteHeader(http.StatusOK)
		c.Response.Write(b)
	}
	log.Warn("controller Response: %#v", err)
}

func (c *Controller) WriteXml(replaceString string) {
	bytes, err := xml.Marshal(c.Result)
	if err != nil {
		log.Warn("xml编码失败，原因: %#v", err)
		return
	}
	if len(replaceString) >= 0 {
		strResp := strings.Replace(string(bytes), replaceString, "xml", -1)
		bytes = []byte(strResp)
	}

	c.Response.Header().Set("Content-Type", "application/xml")
	c.Response.WriteHeader(http.StatusOK)
	c.Response.Write(bytes)
}
