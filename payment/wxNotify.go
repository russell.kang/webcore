package payment

import (
	"strings"
	"encoding/base64"
	"encoding/xml"
	"io/ioutil"
	"net/http"

	"pkg/setting"
	"pkg/util"

	log "gopkg.in/clog.v1"
)

type WxRefundNotify struct {
	WxReturn

	// 以下字段在return_code为SUCCESS的时候有返回
	Appid    string `xml:"appid"`
	MchId    string `xml:"mch_id"`
	NonceStr string `xml:"nonce_str"`
	ReqInfo  string `xml:"req_info"` // String(1024) 加密信息请用商户秘钥进行解密，详见解密方式
}

type WxRefundContent struct {
	// 以下为返回的加密字段：
	// 微信订单号	String(32)	微信订单号
	TransactionId string `xml:"transaction_id"`
	// 商户订单号	String(32)	商户系统内部的订单号
	OutTradeNo string `xml:"out_trade_no"`
	// 微信退款单号	String(32)	微信退款单号
	RefundId string `xml:"refund_id"`
	// 商户退款单号	String(64)	商户退款单号
	OutRefundNo string `xml:"out_refund_no"`
	// 订单金额	Int	订单总金额，单位为分，只能为整数，详见支付金额
	TotalFee float64 `xml:"total_fee"`
	// 应结订单金额	Int	当该订单有使用非充值券时，返回此字段。应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额。
	SettlementTotalFee float64 `xml:"settlement_total_fee"`
	// 申请退款金额	Int	退款总金额,单位为分
	RefundFee float64 `xml:"refund_fee"`
	// 退款金额	Int	退款金额=申请退款金额-非充值代金券退款金额，退款金额<=申请退款金额
	SettlementRefundFee float64 `xml:"settlement_refund_fee"`
	// 退款状态	String(16)	"SUCCESS-退款成功, CHANGE-退款异常, REFUNDCLOSE—退款关闭"
	RefundStatus string `xml:"refund_status"`
	// 退款成功时间	String(20)	资金退款至用户帐号的时间，格式2017-12-15 09:46:01
	SuccessTime string `xml:"success_time"`
	// 退款入账账户	String(64)	取当前退款单的退款入账方, 1）退回银行卡：{银行名称}{卡类型}{卡尾号}, 2）退回支付用户零钱:支付用户零钱, 3）退还商户:商户基本账户, 商户结算银行账户, 4）退回支付用户零钱通:支付用户零钱通
	RefundRecvAccout string `xml:"refund_recv_accout"`
	// 退款资金来源	String(30)	"REFUND_SOURCE_RECHARGE_FUNDS 可用余额退款/基本账户, REFUND_SOURCE_UNSETTLED_FUNDS 未结算资金退款"
	RefundAccount string `xml:"refund_account"`
	// 退款发起来源	String(30)	"API接口, VENDOR_PLATFORM商户平台"
	RefundRequestSource string `xml:"refund_request_source"`
}

func WxCallBackRefund(request *http.Request) (WxRefundContent, bool) {
	var resp WxRefundNotify
	var result bool = false

	// body
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.Error(0, "读取http body失败，原因: %#v", err)
		return WxRefundContent{}, result
	}
	defer request.Body.Close()

	log.Info("微信退款异步通知，HTTP Body: %s", string(body))

	err = xml.Unmarshal(body, &resp)
	if err != nil {
		log.Error(0, "解析HTTP Body格式到xml失败，原因: %#v", err)
		return WxRefundContent{}, result
	}

	// 数据解密
	return (&resp).RefundNotifyDecode()
}

//微信退款数据解密
func (r *WxRefundNotify) RefundNotifyDecode() (WxRefundContent, bool) {
	key := strings.ToLower(util.Md5(setting.Payment.WxAppKey))
	content := WxRefundContent{}

	data, err := base64.StdEncoding.DecodeString(r.ReqInfo)
	if err != nil {
		log.Warn("base64 DecodeString Error: %v", err)
		return content, false
	}

	plaintext := util.AesECBDecrypt(data, key)

	err = xml.Unmarshal(plaintext, &content)
	if err != nil {
		log.Error(0, "Unmarshal Error: %#v", err)
		return content, false
	}
	log.Info("WxRefundContent : %+v", content)

	return content, true
}
