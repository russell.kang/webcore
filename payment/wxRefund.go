package payment

import (
	"fmt"

	. "pkg/constant"
	"pkg/setting"
	"pkg/util"
)

//首先定义一个UnifyOrderReq用于填入我们要传入的参数。
type RefundRequest struct {
	Appid         string  `xml:"appid"`
	MchId         string  `xml:"mch_id"`
	NonceStr      string  `xml:"nonce_str"`
	Sign          string  `xml:"sign"`
	TransactionId string  `xml:"transaction_id"`
	OutRefundNo   string  `xml:"out_refund_no"` // String(64), 商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。
	TotalFee      float64 `xml:"total_fee"`     // Int	100	订单总金额，单位为分，只能为整数，详见支付金额
	RefundFee     float64 `xml:"refund_fee"`    // Int	100	退款总金额，订单总金额，单位为分，只能为整数，详见支付金额
	NotifyUrl     string  `xml:"notify_url"`    // String(256) 异步接收微信支付退款结果通知的回调地址，通知URL必须为外网可访问的url，不允许带参数, 如果参数中传了notify_url，则商户平台上配置的回调地址将不会生效。
}

type RefundResponse struct {
	WxResponse

	// String(32)	列表详见错误码列表
	ErrCode string `xml:"err_code"`
	// String(128)	结果信息描述
	ErrCodeDes    string `xml:"err_code_des"`
	Appid         string `xml:"appid"`
	MchId         string `xml:"mch_id"`
	NonceStr      string `xml:"nonce_str"`
	Sign          string `xml:"sign"`
	TransactionId string `xml:"transaction_id"`
	OutTradeNo    string `xml:"out_trade_no"`
	OutRefundNo   string `xml:"out_refund_no"`
	// String(32)	微信退款单号
	RefundId string `xml:"refund_id"`
	// Int	退款总金额,单位为分,可以做部分退款
	refundFee float64 `xml:"refund_fee"`
	// Int	去掉非充值代金券退款金额后的退款金额，退款金额=申请退款金额-非充值代金券退款金额，退款金额<=申请退款金额
	settlementRefundFee float64 `xml:"settlement_refund_fee"`
	// Int	订单总金额，单位为分，只能为整数，详见支付金额
	totalFee float64 `xml:"total_fee"`
	// Int	去掉非充值代金券金额后的订单总金额，应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额。
	settlementTotalFee float64 `xml:"settlement_total_fee"`
	// String(8)	订单金额货币类型，符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	feeType string `xml:"fee_type"`
	// Int	现金支付金额，单位为分，只能为整数，详见支付金额
	cashFee float64 `xml:"cash_fee"`
	// String(16)	货币类型，符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	cashFeeType string `xml:"cash_fee_type"`
	// Int	现金退款金额，单位为分，只能为整数，详见支付金额
	cashRefundFee float64 `xml:"cash_refund_fee"`
}

/**
 * 发起退款
 */
func WxRefund(transactionId string, totalFee float64, refundFee float64) error {
	refundOrderUrl := "https://api.mch.weixin.qq.com/secapi/pay/refund"

	var wxRefund RefundRequest
	wxRefund.Appid = setting.Payment.WxAppId
	wxRefund.MchId = setting.Payment.WxMchId
	wxRefund.NonceStr = util.RandString(32)
	wxRefund.TransactionId = transactionId
	wxRefund.OutRefundNo = util.IdWorker(ORDER_REFUND_PREFIX)
	wxRefund.TotalFee = totalFee
	wxRefund.RefundFee = refundFee
	wxRefund.NotifyUrl = fmt.Sprintf("%s/weixin/notify/refund", setting.Site.URL) //异步返回的地址

	(&wxRefund).SignMD5()

	xmlResp := RefundResponse{}

	body, err := WxXmlRequest(refundOrderUrl, wxRefund, true, "RefundRequest")
	if err != nil {
		return err
	}

	err = (&xmlResp).WxDecode(body)
	if err != nil {
		return err
	}

	return nil

}

//wxpay计算签名的函数
func (r *RefundRequest) SignMD5() {
	signMap := map[string]interface{}{}
	signMap["appid"] = r.Appid
	signMap["mch_id"] = r.MchId
	signMap["nonce_str"] = r.NonceStr
	signMap["transaction_id"] = r.TransactionId
	signMap["out_refund_no"] = r.OutRefundNo
	signMap["total_fee"] = r.TotalFee
	signMap["refund_fee"] = r.RefundFee
	signMap["notify_url"] = r.NotifyUrl

	r.Sign = WxSignMD5(signMap)
}
