package payment

import (
	"fmt"

	. "pkg/constant"
	"pkg/setting"
	"pkg/util"
)

//首先定义一个UnifyOrderReq用于填入我们要传入的参数。
type UnifyOrderReq struct {
	Appid string `xml:"appid"`
	//商品描述 例如: 腾讯充值中心-QQ会员充值
	Body string `xml:"body"`
	// Fee_type         string `xml:"fee_type"`
	MchId          string `xml:"mch_id"`
	NonceStr       string `xml:"nonce_str"`
	NotifyUrl      string `xml:"notify_url"`
	OutTradeNo     string `xml:"out_trade_no"`
	SpbillCreateIp string `xml:"spbill_create_ip"`
	TotalFee       int    `xml:"total_fee"`
	TradeType      string `xml:"trade_type"`
	Sign           string `xml:"sign"`
	SceneInfo      string `xml:"scene_info"`
}

type UnifyOrderResp struct {
	WxResponse

	Attach     string `xml:"attach"`
	Appid      string `xml:"appid"`
	MchId      string `xml:"mch_id"`
	NonceStr   string `xml:"nonce_str"`
	Sign       string `xml:"sign"`
	ErrCode    string `xml:"err_code"`
	ErrCodeDes string `xml:"err_code_des"`
	PrepayId   string `xml:"prepay_id"`
	TradeType  string `xml:"trade_type"`
	CodeUrl    string `xml:"code_url"`
	MwebUrl    string `xml:"mweb_url"`
}


/**
 * 二维码 支付
 */
func Wxpay(param map[string]string) (string, error) {
	unifyOrderUrl := "https://api.mch.weixin.qq.com/pay/unifiedorder"

	var wxReq UnifyOrderReq
	wxReq.Appid = setting.Payment.WxAppId //微信开放平台我们创建出来的app的app id
	wxReq.Body = ORDER_TITLE_INTENTION
	wxReq.MchId = setting.Payment.WxMchId
	wxReq.NonceStr = util.RandString(32)
	wxReq.NotifyUrl = fmt.Sprintf("%s/weixin/notify", setting.Site.URL) //异步返回的地址
	wxReq.TradeType = "NATIVE"
	wxReq.SpbillCreateIp = param["requestIp"]

	wxReq.TotalFee = util.StrToInt(param["feeIntention"]) //单位是分，这里是1毛钱
	wxReq.OutTradeNo = param["outTradeNO"]
	(&wxReq).signMD5()


	body, err := WxXmlRequest(unifyOrderUrl, wxReq, false, "UnifyOrderReq")
	if err != nil {
		return "", err
	}

	xmlResp := new(UnifyOrderResp)
	err = xmlResp.WxDecode(body)
	if err != nil {
		return "", err
	}
	return xmlResp.CodeUrl, nil
}

func (wxReq *UnifyOrderReq) signMD5() {
	signMap := make(map[string]interface{}, 9)

	signMap["appid"] = wxReq.Appid
	signMap["body"] = wxReq.Body
	// signMap["fee_type"] = wxReq.Fee_type
	signMap["mch_id"] = wxReq.MchId
	signMap["nonce_str"] = wxReq.NonceStr
	signMap["notify_url"] = wxReq.NotifyUrl
	signMap["trade_type"] = wxReq.TradeType
	signMap["spbill_create_ip"] = wxReq.SpbillCreateIp
	signMap["total_fee"] = wxReq.TotalFee
	signMap["out_trade_no"] = wxReq.OutTradeNo

	wxReq.Sign = WxSignMD5(signMap) //这个是计算wxpay签名的函数上面已贴出
}
