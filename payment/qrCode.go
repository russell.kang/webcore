package payment

import (
	"encoding/base64"

	"pkg/setting"

	qrcode "github.com/skip2/go-qrcode"
	log "gopkg.in/clog.v1"
)

func WxPayQrCode(qrUrl string) string {
	var image string

	if setting.ProdMode {
		png, err := qrcode.Encode(qrUrl, qrcode.Medium, 256)
		if err != nil {
			log.Warn("WxPayQrCode error:%v, %+v", qrUrl, err)
			return image
		}
		image = "data:image/png;base64," + base64.StdEncoding.EncodeToString(png)
	} else {
		image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAMAAABrrFhUAAAABlBMVEX///8AAABVwtN+AAACMUlEQVR42uzdS47jIBSGUWf/m64FlIT+e0HghPONWq04sc8E8bDqkSRJkiRJkiRJkiRJkiRJ0ieu+vn/V626BwAAAAAAsBag+pnkpsYAOU/+GQAAAAAAMAOQDGLJwJgMdDP3AAAAAAAATgHkA1TCBgAAAAAAvh0gnzBVqQAAAAAAwOdlS2L5pCcZ7r5wTRAAAAAAfhCgt9G99t9fdT4AAAAAAH4EYO3g+bLbBAAAAAAA5SWxZIqTfMP8BsuhNUEAAAAAuAigt6BVHRjzlzDG1wIAAAAAgGfLwtiq/68ejN2+PQ4AAAAAVwP0Jii9icv4qkPTIAAAAAAAUBjKei/V5VMuAAAAAADwHJ0G9ba+qy9e96ZNAAAAAABg51gwc4i1dwh35qUKAAAAAACwanksH5TWvgy3fU8IAAAAAACUj7tWpz7j30o2TDbuDwEAAADAdQAzQ1+VNp8GHR0MAQAAAOAKgHyDonf0Nf+eQ8MgAAAAAFwNkP9I8lhrwca/CwAAAAAA1g6D1YfuLZKNr+od0QIAAAAAAHuaPwybP2g+eQIAAAAAAGuXxJJhrbolkm+5vGBNEAAAAAAuAuh9Jh/cqgtg25fEAAAAAADAgj+8PL8YlhyYAgAAAAAAZwF6h1t7y2kbJ0MAAAAAAKC8xZ3ffo704skQAAAAAPwswMyS2My0acuDAgAAAACAJ9/cqE5KeoehqtsjAAAAAABAkiRJkiRJkiRJkiRJkiTp6v4CAAD//4c/PVH/n5aeAAAAAElFTkSuQmCC"
	}

	return image
}
