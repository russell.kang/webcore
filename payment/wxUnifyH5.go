package payment

import (
	"fmt"

	. "pkg/constant"
	"pkg/setting"
	"pkg/util"
)

/**
 * H5 支付
 */
func WxpayH5(param map[string]string, channel string) string {
	unifyOrderUrl := "https://api.mch.weixin.qq.com/pay/unifiedorder"

	var wxReq UnifyOrderReq
	wxReq.Appid = setting.Payment.WxAppId //微信开放平台我们创建出来的app的app id
	wxReq.Body = ORDER_TITLE_INTENTION
	wxReq.MchId = setting.Payment.WxMchId
	wxReq.NonceStr = util.RandString(32)
	wxReq.NotifyUrl = fmt.Sprintf("%s/weixin/notify", setting.Site.URL) //异步返回的地址
	wxReq.TradeType = "MWEB"
	wxReq.SpbillCreateIp = param["requestIp"]

	wxReq.TotalFee = util.StrToInt(param["feeIntention"]) //单位是分，这里是1毛钱
	wxReq.OutTradeNo = param["outTradeNO"]
	if channel == CHANNEL_ANDROID {
		wxReq.SceneInfo = WxSceneInfoAndroid(ORDER_TITLE_INTENTION)
	} else if channel == CHANNEL_IOS {
		wxReq.SceneInfo = WxSceneInfoIOS(ORDER_TITLE_INTENTION)
	} else {
		return ""
	}
	(&wxReq).signH5MD5()


	body, err := WxXmlRequest(unifyOrderUrl, wxReq, false, "UnifyOrderReq")
	if err != nil {
		return ""
	}

	xmlResp := &UnifyOrderResp{}
	err = xmlResp.WxDecode(body)
	if err != nil {
		return ""
	}

	return xmlResp.MwebUrl
}

func (wxReq *UnifyOrderReq) signH5MD5() {
	signMap := make(map[string]interface{}, 10)

	signMap["appid"] = wxReq.Appid
	signMap["body"] = wxReq.Body
	signMap["mch_id"] = wxReq.MchId
	signMap["nonce_str"] = wxReq.NonceStr
	signMap["notify_url"] = wxReq.NotifyUrl
	signMap["trade_type"] = wxReq.TradeType
	signMap["spbill_create_ip"] = wxReq.SpbillCreateIp
	signMap["total_fee"] = wxReq.TotalFee
	signMap["out_trade_no"] = wxReq.OutTradeNo
	signMap["scene_info"] = wxReq.SceneInfo

	wxReq.Sign = WxSignMD5(signMap) //这个是计算wxpay签名的函数上面已贴出
}
