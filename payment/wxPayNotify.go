package payment

import (
	// "strings"
	"encoding/xml"
	"io/ioutil"
	"net/http"

	// "pkg/setting"

	log "gopkg.in/clog.v1"
)

type WxPayNotifyReq struct {
	Return_code    string `xml:"return_code"`
	Return_msg     string `xml:"return_msg"`
	Appid          string `xml:"appid"`
	Mch_id         string `xml:"mch_id"`
	Nonce          string `xml:"nonce_str"`
	Sign           string `xml:"sign"`
	Result_code    string `xml:"result_code"`
	Openid         string `xml:"openid"`
	Is_subscribe   string `xml:"is_subscribe"`
	Trade_type     string `xml:"trade_type"`
	Bank_type      string `xml:"bank_type"`
	Total_fee      int    `xml:"total_fee"`
	Fee_type       string `xml:"fee_type"`
	Cash_fee       int    `xml:"cash_fee"`
	Cash_fee_Type  string `xml:"cash_fee_type"`
	Transaction_id string `xml:"transaction_id"`
	Out_trade_no   string `xml:"out_trade_no"`
	Attach         string `xml:"attach"`
	Time_end       string `xml:"time_end"`
}

type WxPayNotifyResp struct {
	Return_code string `xml:"return_code"`
	Return_msg  string `xml:"return_msg"`
}

func WxpayCallback(request *http.Request) (WxPayNotifyReq, WxPayNotifyResp, bool) {
	var mr WxPayNotifyReq
	var resp WxPayNotifyResp
	var result bool = false
	reqMap := make(map[string]interface{}, 0)

	// body
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.Error(0, "读取http body失败，原因: %#v", err)
		return mr, resp, result
	}
	defer request.Body.Close()

	log.Info("微信支付异步通知，HTTP Body: %s", string(body))

	err = xml.Unmarshal(body, &mr)
	if err != nil {
		log.Error(0, "解析HTTP Body格式到xml失败，原因: %#v", err)
		return mr, resp, result
	}

	reqMap["return_code"] = mr.Return_code
	reqMap["return_msg"] = mr.Return_msg
	reqMap["appid"] = mr.Appid
	reqMap["mch_id"] = mr.Mch_id
	reqMap["nonce_str"] = mr.Nonce
	reqMap["result_code"] = mr.Result_code
	reqMap["openid"] = mr.Openid
	reqMap["is_subscribe"] = mr.Is_subscribe
	reqMap["trade_type"] = mr.Trade_type
	reqMap["bank_type"] = mr.Bank_type
	reqMap["total_fee"] = mr.Total_fee
	reqMap["fee_type"] = mr.Fee_type
	reqMap["cash_fee"] = mr.Cash_fee
	reqMap["cash_fee_type"] = mr.Cash_fee_Type
	reqMap["transaction_id"] = mr.Transaction_id
	reqMap["out_trade_no"] = mr.Out_trade_no
	reqMap["attach"] = mr.Attach
	reqMap["time_end"] = mr.Time_end

	//进行签名校验
	if WxPayVerifySign(reqMap, mr.Sign) {
		result = true
		//这里就可以更新我们的后台数据库了，其他业务逻辑同理。
		resp.Return_code = "SUCCESS"
		resp.Return_msg = "OK"
	} else {
		resp.Return_code = "FAIL"
		resp.Return_msg = "验证签名失败"
	}

	return mr, resp, result
}

//微信支付签名验证函数
func WxPayVerifySign(needVerifyM map[string]interface{}, sign string) bool {
	signCalc := WxSignMD5(needVerifyM)

	log.Info("计算出来的sign: %#v", signCalc)
	log.Info("微信异步通知sign: %#v", sign)
	if sign == signCalc {
		log.Info("签名校验通过")
		return true
	}

	log.Info("签名校验失败")
	return false
}
