package payment

import (
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"sort"
	"strings"

	log "gopkg.in/clog.v1"

	"pkg/setting"
	"pkg/util"
)

func WxXmlRequest(url string, bytesData interface{}, isTLS bool, replaceStr string) ([]byte, error) {
	//发送unified order请求.
	orderRequest, err := http.NewRequest("POST", url, WxXmlData(bytesData, replaceStr))
	if err != nil {
		log.Warn("New Http Request发生错误, 原因: %#v", err)
		return []byte{}, err
	}
	orderRequest.Header.Set("Accept", "application/xml")
	//这里的http header的设置是必须设置的.
	orderRequest.Header.Set("Content-Type", "application/xml; encoding=UTF-8")

	client := &http.Client{}
	if isTLS { // 设置证书
		// Load client cert using pem is not safe.
		cert, err := tls.LoadX509KeyPair(setting.Payment.WxCertPath, setting.Payment.WxCertKeyPath)
		if err != nil {
			log.Warn("wx cert read error: %#v", err)
			return nil, err
		}

		tlsConfig := &tls.Config{
			Certificates: []tls.Certificate{
				cert}}
		tlsConfig.BuildNameToCertificate()
		client.Transport = &http.Transport{TLSClientConfig: tlsConfig}
	}

	resp, err := client.Do(orderRequest)
	if err != nil {
		log.Warn("请求微信发送错误, 原因: %#v", err)
		return nil, err
	}

	return ioutil.ReadAll(resp.Body)
}

func WxXmlData(data interface{}, replaceStr string) io.Reader {
	xmlReq, err := xml.Marshal(data)
	if err != nil {
		log.Warn("以xml形式编码发送错误, 原因: %#v", err)
	}

	strReq := string(xmlReq)
	//wxpay的unifiedorder接口需要http body中xmldoc的根节点是<xml></xml>这种，所以这里需要replace一下
	strReq = strings.Replace(strReq, replaceStr, "xml", -1)

	log.Info("strReq: %s", strReq)

	return bytes.NewReader([]byte(strReq))
}

type WxReturn struct {
	// SUCCESS/FAIL
	// 此字段是通信标识，非交易标识，交易是否成功需要查看trade_state来判断
	ReturnCode string `xml:"return_code"`
	// 当return_code为FAIL时返回信息为错误原因 ，例如 签名失败
	// 参数格式校验错误
	ReturnMsg string `xml:"return_msg"`
}

// 调用接口后，返回信息
type WxResponse struct {
	WxReturn

	// String(16)
	// SUCCESS/FAIL
	// SUCCESS退款申请接收成功，结果通过退款查询接口查询
	// FAIL 提交业务失败
	ResultCode string `xml:"result_code"`
}

func (xmlResp *RefundResponse) WxDecode(body []byte) error {
	log.Info("微信返回结果 body: %v", string(body))
	err := xml.Unmarshal(body, xmlResp)

	if xmlResp.ReturnCode == "FAIL" {
		log.Warn("微信返回结果 -通信标识- 不成功, 原因: %+v, Err: %#v", xmlResp, err)
		return err
	}
	if xmlResp.ResultCode == "FAIL" {
		log.Warn("微信返回结果 -业务- 不成功, 原因: %+v", xmlResp)
		return err
	}
	//这里已经得到微信支付的prepay id，需要返给客户端，由客户端继续完成支付流程
	log.Info("微信返回结果 成功: %+v", xmlResp)
	return nil
}

func (xmlResp *UnifyOrderResp) WxDecode(body []byte) error {
	log.Info("微信返回结果 body: %v", string(body))
	err := xml.Unmarshal(body, xmlResp)

	if xmlResp.ReturnCode == "FAIL" {
		log.Warn("微信返回结果 -通信标识- 不成功, 原因: %+v, Err: %#v", xmlResp, err)
		return err
	}
	if xmlResp.ResultCode == "FAIL" {
		log.Warn("微信返回结果 -业务- 不成功, 原因: %+v", xmlResp)
		return err
	}
	//这里已经得到微信支付的prepay id，需要返给客户端，由客户端继续完成支付流程
	log.Info("微信返回结果 成功: %+v", xmlResp)
	return nil
}

//wxpay计算签名的函数
func WxSignMD5(signMap map[string]interface{}) string {
	//STEP 1, 对key进行升序排序.
	sortedKeys := []string{}
	for k := range signMap {
		sortedKeys = append(sortedKeys, k)
	}

	sort.Strings(sortedKeys)

	//STEP2, 对key=value的键值对用&连接起来，略过空值
	var signStrings string
	for _, k := range sortedKeys {
		value := fmt.Sprintf("%v", signMap[k])
		if value != "" {
			signStrings += k + "=" + value + "&"
		}
	}

	//STEP3, 在键值对的最后加上key=API_KEY
	signStrings += "key=" + setting.Payment.WxAppKey

	log.Info("signStrings: %s", signStrings)
	//STEP4, 进行MD5签名并且将所有字符转为大写.

	return strings.ToUpper(util.Md5(signStrings))
}

// SceneInfo
// 安卓移动应用
func WxSceneInfoAndroid(title string) string {
	return `{"h5_info": {"type":"Android","app_name": "` +title+ `","package_name": "hc.wancun.com"}}`
}
// IOS移动应用
func WxSceneInfoIOS(title string) string {
	return `{"h5_info": {"type":"IOS","app_name": "` +title+ `","bundle_id": "com.wancun.yylc"}}`
}