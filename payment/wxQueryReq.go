package payment

import (
	"bytes"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"strings"

	"pkg/setting"
	"pkg/util"

	log "gopkg.in/clog.v1"
)

type OrderQueryReq struct {
	AppId      string `xml:"appid"`
	MchId      string `xml:"mch_id"`
	OutTradeNO string `xml:"out_trade_no"`
	NonceStr   string `xml:"nonce_str"`
	Sign       string `xml:"sign"`
}

type OrderQueryResp struct {
	Return_code      string `xml:"return_code"`
	Return_msg       string `xml:"return_msg"`
	Appid            string `xml:"appid"`
	Mch_id           string `xml:"mch_id"`
	Nonce_str        string `xml:"nonce_str"`
	Sign             string `xml:"sign"`
	Result_code      string `xml:"result_code"`
	Err_code         string `xml:"err_code"`
	Err_code_des     string `xml:"err_code_des"`
	Openid           string `xml:"prepay_id"`
	Trade_type       string `xml:"trade_type"`
	Trade_state      string `xml:"trade_state"`
	Bank_type        string `xml:"bank_type"`
	Total_fee        string `xml:"total_fee"`
	Cash_fee         int    `xml:"cash_fee"`
	Transaction_id   string `xml:"transaction_id"`
	Out_trade_no     string `xml:"out_trade_no"`
	Time_end         string `xml:"time_end"`
	Trade_state_desc string `xml:"trade_state_desc"`
}

func WxQueryOrder(outTradeNO string) OrderQueryResp {
	xmlResp := OrderQueryResp{}
	wxQueryOrderUrl := "https://api.mch.weixin.qq.com/pay/orderquery"

	// 请求
	var queryReq OrderQueryReq
	queryReq.AppId = setting.Payment.WxAppId
	queryReq.MchId = setting.Payment.WxMchId
	queryReq.OutTradeNO = outTradeNO
	queryReq.NonceStr = util.RandString(32)

	// key 数据
	signData := make(map[string]interface{}, 0)
	signData["appid"] = queryReq.AppId
	signData["mch_id"] = queryReq.MchId
	signData["out_trade_no"] = queryReq.OutTradeNO
	signData["nonce_str"] = queryReq.NonceStr

	queryReq.Sign = WxSignMD5(signData)

	reqBytes, err := xml.Marshal(queryReq)
	if err != nil {
		log.Warn("以xml形式编码发送错误, 原因: %#v", err)
		return xmlResp
	}

	reqStr := strings.Replace(string(reqBytes), "OrderQueryReq", "xml", -1)
	reqBytes = []byte(reqStr)

	// http 微信请求
	req, err := http.NewRequest("POST", wxQueryOrderUrl, bytes.NewReader(reqBytes))
	if err != nil {
		log.Warn("http.NewRequest 发生错误，原因: %#v", err)
		return xmlResp
	}

	req.Header.Set("Accept", "application/xml")
	//这里的http header的设置是必须设置的.
	req.Header.Set("Content-Type", "application/xml; encoding=UTF-8")

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Warn("http.Client Do 错误: %#v", err)
		return xmlResp
	}

	body, _ := ioutil.ReadAll(resp.Body)
	err = xml.Unmarshal(body, &xmlResp)
	if xmlResp.Return_code == "FAIL" {
		log.Error(0, "微信支付查询失败, 原因: %s, Err: %#v", xmlResp.Return_msg, err)
		return xmlResp
	}
	if xmlResp.Result_code == "FAIL" {
		log.Error(0, "微信支付查询失败, 业务原因: %s, %s", xmlResp.Err_code, xmlResp.Err_code_des)
		return xmlResp
	}
	//这里已经得到微信支付的prepay id，需要返给客户端，由客户端继续完成支付流程
	// log.Info("微信支付查询成功，预支付单号: %s, %s", xmlResp.Prepay_id, xmlResp.Code_url)
	return xmlResp
}
