package constant

const (
	/** 后台用户 */
	TABLE_USER = "fiction_user"

	/** 客户 */
	TABLE_CUSTOMER = "fiction_customer"
	/** 邀请信息 */
	TABLE_CUSTOMER_REL = "fiction_customer_rel"
	/** 客户信息 */
	TABLE_CUSTOMER_INFO = "fiction_customer_info"
	/** 客户登录信息表 */
	TABLE_CUSTOMER_TOKEN = "fiction_customer_token"
	/** 客户登录历史信息表 */
	TABLE_CUSTOMER_TOKEN_HISTORY = "fiction_customer_token_history"

	/** 属性 */
	TABLE_TAG = "fiction_tags"
	/** 图片 */
	TABLE_IMAGE = "fiction_images"

	/** 订单 */
	TABLE_PRODUCT = "fiction_product"
	/** 优惠券 */
	TABLE_PRODUCT_TAG = "fiction_product_tags"
	/** 图片 */
	TABLE_PRODUCT_IMAGE = "fiction_product_image"
	/** 二维码支付信息 */
	TABLE_PRODUCT_PAYMENT = "fiction_product_payment"
)
