package constant

const (
	VERIFY_SECONDS = 1800
	// 一天的秒数
	VERIFY_DAYS = 86400

	// 验证发送次数限制
	VERIFY_DAY_LIMIT    = 50 // 一天 50 条
	VERIFY_HOUR_LIMIT   = 6  // 一小时 6 条
	VERIFY_MINUTE_LIMIT = 2  // 一分钟 2 条
)
