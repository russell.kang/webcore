package constant

const (
	CHANNEL_MGR = "manager"

	CHANNEL_IOS     = "ios"
	CHANNEL_ANDROID = "android"
	CHANNEL_WEB     = "web"

	SECRET_IOS     = "4ffa40ddde003dcbd76e442f59696743"
	SECRET_ANDROID = "53bfc031aac9a661f8fa5358e1a157bc"
	SECRET_WEB     = "1612a455563855497d9175ff83cf3d46"
)
