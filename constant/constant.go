package constant

const (
	TIME_FORMAT_REGULAR        = "2006-01-02 15:04:05"
	TIME_FORMAT_REGULAR_SECOND = "2006-01-02 15:04"
	TIME_FORMAT_REGULAR_HOUR   = "2006-01-02 15"
	TIME_FORMAT_REGULAR_DAY    = "2006-01-02"
	TIME_FORMAT_REGULAR_MONTH  = "2006-01"
)
