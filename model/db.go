package model

import (
	"database/sql"

	. "iering.com/webcore/core.zq/constant"
	"iering.com/webcore/core.zq/def"

	"github.com/go-redis/redis"
	_ "github.com/go-sql-driver/mysql"
	log "gopkg.in/clog.v1"
)

type DBTransation struct {
	CommitCode int
	Tx         *sql.Tx
}

// 初始化 并 启用事务
func (commit *DBTransation) Initialize() {
	commit.CommitCode = CODE_OK

	var err error
	commit.Tx, err = Mydb.Begin()
	if err != nil {
		commit.CommitCode = CODE_FAILED
	}
}

// 事务执行与回滚
func (commit *DBTransation) Defer() {
	if commit.CommitCode == CODE_OK {
		commit.Tx.Commit()
	} else {
		commit.Tx.Rollback()
	}
}

// 存储过程 --- 写入
func (commit *DBTransation) Insert(insertSql string, insertValues ...interface{}) int64 {
	stmt, err := commit.Tx.Prepare(insertSql)
	if err != nil {
		commit.CommitCode = CODE_FAILED
		log.Warn("insert trans Prepare : %#v, %v", err, insertSql)
	}
	result, err := stmt.Exec(insertValues...)
	if err != nil {
		commit.CommitCode = CODE_FAILED
		log.Warn("insert trans Exec : %#v, %v, %v", err, insertSql, insertValues)
	}

	id, err := result.LastInsertId()
	if err != nil {
		commit.CommitCode = CODE_FAILED
		log.Warn("insert trans LastInsertId : %#v", err)
	}

	return id
}

func (commit *DBTransation) Update(updateSql string, updateValues ...interface{}) {
	stmt, err := commit.Tx.Prepare(updateSql)
	if err != nil {
		commit.CommitCode = CODE_FAILED
		log.Warn("Update trans Prepare : %#v", err)
	}
	result, err := stmt.Exec(updateValues...)
	if err != nil {
		commit.CommitCode = CODE_FAILED
		log.Warn("Update trans Exec : %#v, %v, %v", err, updateSql, updateValues)
	}

	_, err = result.RowsAffected()
	if err != nil {
		commit.CommitCode = CODE_FAILED
		log.Warn("Update trans RowsAffected : %#v", err)
	}
}

var Mydb *sql.DB
var MyRedis *redis.Client

func NewContext() {
	/** Mysql */
	Mydb = initMysql()

	/** Redis */
	MyRedis = initRedis()
}

func initMysql() *sql.DB {
	db, err := sql.Open(def.Db.Driver, def.Db.Dsn)
	if err != nil {
		log.Fatal(3, "Open db fail! %#v", err)
	}
	db.SetMaxOpenConns(1000)
	db.SetMaxIdleConns(1000)

	return db
}

func initRedis() *redis.Client {
	/** Redis */
	goRedis := redis.NewClient(&redis.Options{
		Addr:     def.Redis.HostServer,
		Password: def.Redis.Password,
		DB:       def.Redis.Db,
	})

	_, err := goRedis.Ping().Result()
	if err != nil {
		log.Fatal(3, "Open redis fail! %#v", err)
	}

	return goRedis
}

