package upload

import (
	"io"
	"os"
	"io/ioutil"
	"time"
	"mime/multipart"
	"net/http"
	"errors"
	"crypto/sha1"
	"path/filepath"
	"fmt"
	"strings"

	"iering.com/webcore/core.zq/util"
	"iering.com/webcore/core.zq/token"

	log "gopkg.in/clog.v1"
)

var ErrNoFile = errors.New("客户端没有上传文件")

type (
	IDBUploader interface {
		/** 根据FileHash检查数据库是否存在 */
		CheckFile() bool

		/** 数据库存储 */
		DbStore() bool
	}
	Uploaders []Uploader
	Uploader struct {
		/** 文件访问地址 比如: http://image.ggg.com/	 */
		BaseUrl string
		/** 存储文件根目录 比如: /usr/local/www/datas/ */
		BasePath string
		/** 存储文件目录 比如: 20180829/ */
		FilePath string
		/** 存储文件全地址 比如: 20180829/123456.jpg */
		FileName string
		/** 上传文件最大值, ParseMultipartForm将请求的主体作为multipart/form-data解析。请求的整个主体都会被解析，得到的文件记录最多maxMemery字节保存在内存，其余部分保存在硬盘的temp文件里。如果必要，ParseMultipartForm会自行调用ParseForm。重复调用本方法是无意义的。 */
		MaxMemory int64
		/** 上传文件类型 */
		Exts []string

		InputField string
		Request *http.Request

		/** 上传文件信息 */
		OriginFileName string
		StoreFileName string
		FileExt string
		FileHash string
		BufferFile []byte

		/** 返回信息 */
		FileId int64
		Form map[string]string
	}

	Result struct {
		Id int64 `json:"id"`
		Url string `json:"url"`
	}
)

func New(baseUrl, basePath string, maxMemory int64, exts []string, inputField string, r *http.Request) *Uploader {
	uf := new(Uploader)
	uf.BaseUrl = baseUrl
	uf.BasePath = basePath
	uf.MaxMemory = maxMemory
	uf.Exts = exts

	uf.InputField = inputField
	uf.Request = r

	uf.Form = make(map[string]string)

	return uf
}

// ParseMultipartForm
func (uf *Uploader) DoForm() (Uploaders, error) {
	result := Uploaders{}
	if err := uf.Request.ParseMultipartForm(uf.MaxMemory); err != nil {
		return result, err
	}

	if uf.Request.MultipartForm == nil || uf.Request.MultipartForm.File == nil {
		return result, ErrNoFile
	}

	fileHeader := uf.Request.MultipartForm.File[uf.InputField]
	if len(fileHeader) <= 0 {
		return result, ErrNoFile
	}

	uf.formValue(uf.Request.MultipartForm.Value)
	result = uf.workingForm(fileHeader)

	return result, nil
}
// MultipartReader ReadForm
func (uf *Uploader) DoRead() (Uploaders, error) {
	result := Uploaders{}

	mr, err := uf.Request.MultipartReader()
	if err != nil {
		return result, err
	}

	form, err := mr.ReadForm(uf.MaxMemory)
	if err != nil {
		return result, err
	}

	fileHeader := form.File[uf.InputField]
	if len(fileHeader) <= 0 {
		return result, ErrNoFile
	}

	uf.formValue(form.Value)
	result = uf.workingForm(fileHeader)

	return result, nil

}
// MultipartReader NextPart
func (uf *Uploader) DoPart() (Uploaders, error) {
	result := Uploaders{}

	mr, err := uf.Request.MultipartReader()
	if err != nil {
		return result, err
	}

	for {
		part, err := mr.NextPart()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Warn("NextPart error: %v", err)
			break
		}

		uf.BufferFile, err = ioutil.ReadAll(part)
		if err != nil {
			log.Warn("ioutil.ReadAll : %#v", err)
			continue
		}

		if uf.InputField == part.FormName() {
			uf.OriginFileName = part.FileName()
			uf.FileExt = strings.ToLower(filepath.Ext(uf.OriginFileName))
			if !uf.isAllowExt() {
				log.Warn("不允许的文件上传类型: %s", uf.FileExt)
				continue
			}
			// 新的文件名
			uf.StoreFileName = util.NumberWorker() + uf.FileExt
			// 文件Hash值
			uf.FileHash = fmt.Sprintf("%x", sha1.Sum(uf.BufferFile))

			result = append(result, *uf)
		} else {
			uf.Form[part.FormName()] = string(uf.BufferFile)
		}

	}

	return result, nil
}

// 解析表单以为的数据
func (uf *Uploader) formValue(formVal map[string][]string) {
	for k, v := range formVal {
		uf.Form[k] = v[0]
	}
}

func (uf *Uploader) GetUid() int64 {
	tokenString, ok := uf.Form["token"]
	if ok {
		return util.StrToInt64(token.GetField(tokenString, "uid"))
	}
	return 0
}

func (uf *Uploader) workingForm(fileHeaders []*multipart.FileHeader) Uploaders {
	result := Uploaders{}
	for _, file := range fileHeaders {
		if file.Size > uf.MaxMemory {
			log.Warn("文件(%s)上传大小超过最大设定值(%d)", file.Filename, uf.MaxMemory)
			continue
		}

		uf.OriginFileName = file.Filename
		uf.FileExt = strings.ToLower(filepath.Ext(uf.OriginFileName))
		if !uf.isAllowExt() {
			log.Warn("不允许的文件上传类型: %s", uf.FileExt)
			continue
		}

		// 新的文件名
		uf.StoreFileName = util.NumberWorker() + uf.FileExt

		OpenFile, err := file.Open()
		if err != nil {
			log.Warn("file.Open : %#v", err)
			continue
		}
		defer OpenFile.Close()

		uf.BufferFile, err = ioutil.ReadAll(OpenFile)
		if err != nil {
			log.Warn("ioutil.ReadAll : %#v", err)
			continue
		}
		// 文件Hash值
		uf.FileHash = fmt.Sprintf("%x", sha1.Sum(uf.BufferFile))

		result = append(result, *uf)
	}

	return result
}

// 文件写入
func (uf *Uploader) MoveFile() error {
	dst := uf.FileFolder()

	// write file
	newFile, err := os.Create(dst + uf.StoreFileName)
	if err != nil {
		log.Warn("MoveFile Create:%v", err)
		return err
	}
	defer newFile.Close()

	if _, err := newFile.Write(uf.BufferFile); err != nil {
		log.Warn("MoveFile Write:%v", err)
		return err
	}

	if len(uf.FileName) == 0 {
		uf.FileName = uf.FilePath + uf.StoreFileName
	}

	return nil
}

// 上传目录检查
func (uf *Uploader) FileFolder() string {
	uf.FilePath = time.Now().Format("20060102/")

	folder := uf.BasePath + uf.FilePath

	// 判断文件夹是否存在
	if !util.IsExist(folder) {
		// 创建文件夹
		err := os.MkdirAll(folder, os.ModePerm)
		if err != nil {
			log.Warn("upload getImageFolder : %#v", err)
		}
	}

	return folder
}

// 上传类型检查
func (uf *Uploader) isAllowExt() bool {
	if len(uf.Exts) == 0 { // 没有扩展名，一律过滤
		return false
	}

	for _, e := range uf.Exts {
		if e == uf.FileExt {
			return true
		}
	}
	return false
}
