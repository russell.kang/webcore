package token

import (
	"time"

	. "iering.com/webcore/core.zq/constant"
	"iering.com/webcore/core.zq/def"
	"iering.com/webcore/core.zq/model"

	log "gopkg.in/clog.v1"
)

func updateExpired(token string) bool {
	expire := time.Duration(def.TokenExpired) * time.Second

	log.Info("Expire : %#v", expire)

	result := model.MyRedis.Expire(token, expire).Val()

	return result
}

/**
 * 根据token获取账号信息
 * return interface {}
 */
func GetField(token, field string) string {
	result, err := model.MyRedis.HMGet(token, field).Result()

	if err != nil {
		log.Error(0, "token GetField: %#v", err)
		return ""
	}

	return result[0].(string)
}

/**
 * 检查token是否过期
 * return int
 */
func Has(token, channel string) int {
	if len(token) < 1 {
		return CODE_TOKEN_EMPTY
	}

	result, err := model.MyRedis.Exists(token).Result()

	if err != nil {
		log.Error(0, "redis查询token失败: %#v", err)
		return CODE_TOKEN_REDIS
	}

	if result == 1 {
		redisChannel := GetField(token, "channel")

		// updateExpired(token)
		if redisChannel == channel {
			return CODE_OK
		} else {
			return CODE_CHANNEL
		}
	}

	return CODE_TOKEN_EXPIRED
}

/**
 *
 */
func Add(uid int64, channel, account, token string) bool {
	value := make(map[string]interface{})
	value["uid"] = uid
	value["channel"] = channel
	value["account"] = account
	value["ctime"] = time.Now().Unix()

	err := model.MyRedis.HMSet(token, value).Err()
	if err != nil {
		log.Error(0, "token Add: %#v", err)
		return false
	}

	updateExpired(token)

	return true
}

func Delete(token string) bool {
	err := model.MyRedis.Del(token).Err()

	if err != nil {
		log.Error(0, "token Delete: %#v", err)
		return false
	}

	return true
}
